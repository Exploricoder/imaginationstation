# Imagination Station

A multi platform application to allow creative soles to collate notes for imaginary settings for use in works of fiction such as novels, computer games or roleplaying games.

## Getting Started

This solution is designed to run in Visual Studio 2017 Community Edition or higher. You must have the Xamarin (mobile development) feature installed. The solution file is located in the source folder will include projects for all targeted platforms once completed.

### Prerequisites

Visual Studio 2017 Community Edition or higher with Xamarin mobile development features.

### Installing

Simply open up the solution file (source\Exploricoder.ImaginationStation.sln) and build!

## Running the tests

TBA

### Break down into end to end tests

TBA

### And coding style tests

TBA

## Deployment

TBA

## Built With

TBA

## Contributing

TBA

## Versioning

Current Version is 0.1 Pre-Alpha

## Authors

* **Richard Brian Lloyd** - *Initial work* - [The Exploricoder](https://exploricoder.wordpress.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

TBA
