﻿// Copyright(c) 2018 Richard Brian Lloyd (MIT License)
using exploricoder.ImaginationStation.Core.DataObjects;

namespace Exploricoder.ImaginationStation.Core.Interfaces
{
    /// <summary>
    /// Interface for a generic repository used for storage and retrieval of setting objects.
    /// </summary>
    /// <seealso cref="Exploricoder.ImaginationStation.Core.Interfaces.IRepository{exploricoder.ImaginationStation.Core.DataObjects.Settings}" />
    public interface ISettingRepository : IRepository<Settings>
    {
    }
}
