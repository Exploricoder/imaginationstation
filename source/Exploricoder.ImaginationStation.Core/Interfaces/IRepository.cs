﻿
// Copyright(c) 2018 Richard Brian Lloyd (MIT License)
using System.Collections.Generic;

namespace Exploricoder.ImaginationStation.Core.Interfaces
{
    /// <summary>
    /// Interface for a generic repository used for data storage and retrieval.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public interface IRepository<TModel>
    {
        bool Upsert(TModel model);
    }
}
